import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  html:any="Login";
  loginForm: FormGroup;
  submited:boolean=false;
  username = new FormControl('', Validators.required);
  password = new FormControl('', Validators.compose([Validators.required,
              Validators.maxLength(10), Validators.minLength(6)]));
  slideConfig2 = {
    className: 'center',
    centerMode: true,
    infinite: true,
    centerPadding: '0',
    slidesToShow: 1,
    speed: 500,
    dots: true,
  };

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService) {
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: this.username,
      password: this.password
    });
  }

  onSubmit() {
    this.submited=true;
    this.html='<i class="fa fa-spin fa-spinner"></i>Wait...';
    const data = this.loginForm.value;
    const credentials = {
      email: data.username,
      password: data.password,
      company_id: '2',
      user_type:'admin'
    };

    if (this.loginForm.valid) {
      this.authService.loginUser(credentials).subscribe( res => {
        this.submited=false;
        if(res){
          if (!res.status) {
            this.html='Login';
            this.showDanger(res.message);
            this.submited=false;
          } else {
            this.showSuccess(res.message);
            res.data.token = res.token;
            localStorage.setItem('currentUser', JSON.stringify(res.data));
            localStorage.setItem('accessToken', JSON.stringify(res.token));
            this.router.navigate(['/dashboard']);
            this.authService.setLoggedIn(true);
          }
      }else{
        this.html='Login';
        this.submited=false;
        this.showDanger('Wrong Credintials');
      }

      }, error => {
        this.submited=false;
        this.html='Login';
        this.showDanger(error.error.message);
      });
    } else {
      this.submited=false;
      this.html='Login';
      this.showDanger('Please fill the login credential');
    }
  }

  // ------------ Toast message ------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'Success!');
  }

  showDanger(message) {
    this.toastr.warning(message, 'Alert!');
  }

  // ------------ End Toast message ------------------------------//

}
