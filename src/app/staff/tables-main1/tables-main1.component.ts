import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EquipmentListService } from 'src/app/service/equipment-list.service';

@Component({
  selector: 'app-tables-main',
  templateUrl: './tables-main1.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: []
})
export class TablesMain1Component implements OnInit {

  heading = 'Bootstrap 4 Tables';
  subheading = 'Tables are the backbone of almost all web applications.';
  icon = 'pe-7s-drawer icon-gradient bg-happy-itmeo';

  html: any = "Submit";
  staffForm: FormGroup;
  submited: boolean = false;
  tabList: any = [];
  page: any = 1;
  filter: any = {};
  date: any;
  header: string;
  is_edit:string = 'false';
  equipment_id:string='';
  private modalRef;
  constructor(
    private fb: FormBuilder,
    private service: EquipmentListService,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private router: Router,
    private toastr: ToastrService) {
  }
  editStaff(data, c) {

    this.is_edit='true';
    this.header = 'Edit Component'

    // alert(JSON.stringify(data));
    this.staffForm.controls["name"].setValue(data.name);
    if (data.parent) {
      let parents_name = data.parent.name;
      this.equipment_id=data.id;
      // alert(parents_name);
      switch (parents_name) {
        case 'AFTC':
          this.staffForm.controls["parent_id"].setValue(29);
          break;
        case 'DC Track Circuit':
          this.staffForm.controls["parent_id"].setValue(28);
          break;
        case 'MACLS':
          this.staffForm.controls["parent_id"].setValue(30);
          break;
        case 'Point Machine':
          this.staffForm.controls["parent_id"].setValue(27);
          break;
        default:
          break;
      }
    }
    else this.staffForm.controls["parent_id"].setValue('');

    this.modalRef = this.modalService.open(c, { size: 'sm' });
  }
  ngOnInit() {
    this.staffForm = this.fb.group({
      parent_id: [''],
      name: ['', Validators.required],
      is_active: ['1', Validators.required],
      iframe: [''],
    });
    this.getList();

  }
  getList() {
    this.service.getList(this.page, this.filter, this.date).subscribe((res: any) => {
      if (res.status) {
        this.tabList = res.data.data;
        console.log(this.tabList);
      } else {
        this.showDanger(res.message);
      }
    })
  }
  del(content) {

    this.service.delete(content).subscribe(res => {
      this.getList();
    })
  }
  deactivate(content) {
    this.service.dactivate(content).subscribe(res => {
      this.getList();
    })
  }
  activate(content) {
    this.service.activate(content).subscribe(res => {
      this.getList();
    })
  }

  openLg(content) {
    console.log(content);
    this.is_edit='false';
    this.header = 'Add New Component'
    this.modalRef = this.modalService.open(content, { size: 'sm' });
  }

  refresh(){

    this.showSuccess('Refreshed Succesfully');
    this.getList();

  }
  close() {
  }
  onSubmit() {

      if (this.is_edit == 'true'){
        const data = this.staffForm.value;
        this.service.update(data,this.equipment_id).subscribe(res => {
          this.submited = false;
          if (res) {
            if (!res.status) {
              this.html = 'Submit';
              this.showDanger(res.message);
            } else {
              this.html = 'Submit';
              this.showSuccess(res.message);
              this.modalService.dismissAll();
              this.getList();
            }
          } else {
            this.html = 'Submit';
            this.submited = false;
            this.showDanger(res.message);
          }
  
        }, error => {
          this.submited = false;
          this.html = 'Submit';
          this.showDanger(error.error.message);
        });


      }else{

 this.submited = true;
    this.html = '<i class="fa fa-spin fa-spinner"></i>Please Wait...';
    const data = this.staffForm.value;
    console.log(data);

    if (this.staffForm.valid) {
      this.service.add(data).subscribe(res => {
        this.submited = false;
        if (res) {
          if (!res.status) {
            this.html = 'Submit';
            this.showDanger(res.message);
          } else {
            this.html = 'Submit';
            this.showSuccess(res.message);
            this.modalService.dismissAll();
            this.getList();
          }
        } else {
          this.html = 'Submit';
          this.submited = false;
          this.showDanger(res.message);
        }

      }, error => {
        this.submited = false;
        this.html = 'Submit';
        this.showDanger(error.error.message);
      });
    } else {
      this.submited = false;
      this.html = 'Submit';
      this.showDanger('Please fill the required field');
    }
      }
   
  }

  // ------------ Toast message ------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'Success!');
  }

  showDanger(message) {
    this.toastr.warning(message, 'Alert!');
  }

  // ------------ End Toast message ------------------------------//

}
