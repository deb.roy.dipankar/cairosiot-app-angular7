import {Component, OnInit} from '@angular/core';
import {ThemeOptions} from '../../../../../theme-options';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-user-box',
  templateUrl: './user-box.component.html',
})
export class UserBoxComponent implements OnInit {
  toggleDrawer() {
    this.globals.toggleDrawer = !this.globals.toggleDrawer;
  }

  constructor(public globals: ThemeOptions,
    private authService: AuthService) {
      this.authService.loginCheck();
  }

  ngOnInit() {
  }
  logout(){
    this.authService.logout();
  }
}
