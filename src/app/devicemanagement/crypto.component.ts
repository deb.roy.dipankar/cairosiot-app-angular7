import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/service/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BackendApiService } from 'src/app/service/backend-api.service';

@Component({
  selector: 'app-crypto',
  templateUrl: './crypto.component.html',
  styles: []
})
export class DeviceManagementComponent implements OnInit {

  html: any = "Submit";

  heading = 'Crypto Dashboard';
  subheading = 'This is an example dashboard created using build-in elements and components.';
  icon = 'pe-7s-plane icon-gradient bg-tempting-azure';
  staffForm: FormGroup;

  tabList:any = [];
  stationList:any = [];
  locationList:any = [];
  submited: boolean = false;


  private modalRef;
  constructor(
    private fb: FormBuilder,
    private service: BackendApiService,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private router: Router,
    private toastr: ToastrService) {
  }
 

  ngOnInit() {

    this.staffForm = this.fb.group({
      station: [''],
      location: [''],
      device_id: [''],
      name: ['', Validators.required],
      icon: [''],
      lat: [''],
      lon: [''],
      no_of_cycles: [''],
      make: [''],
      model: [''],
      mqtt_ip: [''],
      mqtt_port: [''],
      mqtt_topic: [''],
      time_variation: [''],
      data_variation: [''],
      is_active: [''],

    });
    this.listdevice();
    this.liststation();
 
  }



onOptionsSelected(value:string){
  console.log("the selected value is " + value);


  if(value != ''){

    this.service.listlocation(value).subscribe( (res:any) => {
      if(res.status){
        this.locationList = res.data;
      }else{
        this.showDanger(res.message);
      }
    })

  }else{
    this.locationList = '';
  }
   
}



  listdevice(){
    console.log("-----");
    this.service.listdevice().subscribe( (res:any) => {
      if(res.status){
        this.tabList = res.data.data;
      }else{
        this.showDanger(res.message);
      }
    })
  
  }


  liststation(){
    console.log("-----");
    this.service.liststation().subscribe( (res:any) => {
      if(res.status){
        this.stationList = res.data;
      }else{
        this.showDanger(res.message);
      }
    })
  
  }

  onSubmit(){

    const data = this.staffForm.value;
    this.service.add(data).subscribe(res => {
      this.submited = false;
      if (res) {
        if (!res.status) {
          this.html = 'Submit';
          this.showDanger(res.message);
        } else {
          this.html = 'Submit';
          this.showSuccess(res.message);
          this.modalService.dismissAll();
          this.listdevice();
        }
      } else {
        this.html = 'Submit';
        this.submited = false;
        this.showDanger(res.message);
      }

    }, error => {
      this.submited = false;
      this.html = 'Submit';
      this.showDanger(error.error.message);
    });
  }

  openLg(content) {
    console.log(content);
  
    this.modalRef = this.modalService.open(content, { size: 'sm' });
  }


   // ------------ Toast message ------------------------------//
   showSuccess(message) {
    this.toastr.success(message, 'Success!');
  }

  showDanger(message) {
    this.toastr.warning(message, 'Alert!');
  }

  // ------------ End Toast message ------------------------------//

}
