import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EquipmentListService {

  getHeader(){
    return { headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer '+ JSON.parse(localStorage.getItem('currentUser')).token
    })
  }
  };
  getHeaderImage(){
    return {
        headers: new HttpHeaders({
        'Authorization': 'Bearer ' + (localStorage.getItem("currentUser")?JSON.parse(localStorage.getItem("currentUser"))['token']:'')
      })
    }
  }
  constructor(private http: HttpClient
    ) { 

  }

  // Getting all staff record
  getStaffData():Observable<any> {
    return this.http.get(environment.appConfig.apiUrl + 'staff/is_active=1', this.getHeader());
  }
  
  // Get staff id
  getStaffId(staff_id):Observable<any> {
    return this.http.get(environment.appConfig.apiUrl + 'staff/' + staff_id, this.getHeader());
  }

  // Show Staff data
  getList(page=1,searchParams=null,date=null) {
    const params = {};
    if(page){
      params["page"] = page;
    }
    if(searchParams.length){
      for(let param in searchParams){
        params[param] = searchParams[param];
      }
    }
    if(date){
      params["date"] = date;
    }
    var queryString = "";
    return this.http.get(environment.appConfig.apiUrl + 'equipment-list' + queryString,this.getHeader());
  }

// detail Staff
det(id):Observable<any> {
  return this.http.get(environment.appConfig.apiUrl + 'equipment-details/'+id,this.getHeader());
}

  // update Staff
  update(updateData,id):Observable<any> {
    return this.http.put(environment.appConfig.apiUrl + 'equipment-update/'+id, updateData, this.getHeader());
  }

  
  // Adding Staff
  add(newequipment):Observable<any> {
    return this.http.post(environment.appConfig.apiUrl + 'equipment-add', newequipment, this.getHeader());
  }

  // Deleting staff
  delete(staff_id):Observable<any>  {
    return this.http.delete(environment.appConfig.apiUrl + 'equipment-delete/'+ staff_id , this.getHeader());
  }
  // Dactivate staff
  dactivate(staff_id):Observable<any>  {
    
    console.log(environment.appConfig.apiUrl + 'equipment-activate/' + staff_id);
    return this.http.put(environment.appConfig.apiUrl + 'equipment-deactivate/' + staff_id ,'', this.getHeader());
  }

  // Activate staff
  activate(staff_id):Observable<any>  {
    console.log(environment.appConfig.apiUrl + 'equipment-activate/' + staff_id);
    return this.http.put(environment.appConfig.apiUrl + 'equipment-activate/' + staff_id ,'', this.getHeader());
  }

}
