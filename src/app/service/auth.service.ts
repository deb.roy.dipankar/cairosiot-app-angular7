import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // private loggedInStatus = false;
  private loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  constructor(private http: HttpClient,
    private router: Router) {}

  setLoggedIn(value: boolean) {
    this.loggedInStatus = value;
    localStorage.setItem('loggedIn', 'true');
  }

  // checking is logged in
  get isLoggedIn() {
    // return this.loggedInStatus;
    return JSON.parse(localStorage.getItem('loggedIn') || this.loggedInStatus.toString());
  }
  // get user
  getUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  // checking is logged in
  loginCheck() {
    if(!localStorage.getItem('loggedIn')){    
      this.router.navigate(['/login']);
    }
  }
  // Login Authentication function
  loginUser(credentials:any): Observable<any> {
    return this.http.post(environment.appConfig.apiUrl + 'login', credentials );
  }

  // Logout Function
  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
    // localStorage.removeItem('currentUser');
  }
}
