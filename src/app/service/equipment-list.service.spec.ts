import { TestBed } from '@angular/core/testing';

import { EquipmentListService } from './equipment-list.service';

describe('EquipmentListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EquipmentListService = TestBed.get(EquipmentListService);
    expect(service).toBeTruthy();
  });
});
