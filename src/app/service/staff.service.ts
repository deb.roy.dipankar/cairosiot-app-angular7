import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class StaffService {

  getHeader(){
    return { headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer '+ JSON.parse(localStorage.getItem('currentUser')).token
    })
  }
  };
  getHeaderImage(){
    return {
        headers: new HttpHeaders({
        'Authorization': 'Bearer ' + (localStorage.getItem("currentUser")?JSON.parse(localStorage.getItem("currentUser"))['token']:'')
      })
    }
  }
  constructor(private http: HttpClient
    ) { 

  }

  // Getting all staff record
  getStaffData():Observable<any> {
    return this.http.get(environment.appConfig.apiUrl + 'staff', this.getHeader());
  }
  
  // Get staff id
  getStaffId(staff_id):Observable<any> {
    return this.http.get(environment.appConfig.apiUrl + 'staff/' + staff_id, this.getHeader());
  }

  // Show Staff data
  getStaffList(page=1,searchParams=null,date=null) {
    const params = {};
    if(page){
      params["page"] = page;
    }
    if(searchParams.length){
      for(let param in searchParams){
        params[param] = searchParams[param];
      }
    }
    if(date){
      params["date"] = date;
    }
    var queryString = "?"+Object.keys(params).map(key => key + '=' + params[key]).join('&');
    return this.http.get(environment.appConfig.apiUrl + 'equipment-list' + queryString,this.getHeader());
  }
  // update Staff
  updateStaff(updateData,id):Observable<any> {
    return this.http.put(environment.appConfig.apiUrl + 'staff/'+id + '/update', updateData, this.getHeader());
  }
  
  // Adding Staff
  addStaff(newStaff):Observable<any> {
    return this.http.post(environment.appConfig.apiUrl + 'staff/add', newStaff, this.getHeader());
  }

  // Deleting staff
  deleteStaff(staff_id):Observable<any>  {
    return this.http.delete(environment.appConfig.apiUrl + 'staff/' + staff_id + '/delete', this.getHeader());
  }
  // Deleting staff
  staffStatus(status,type='activate'):Observable<any>  {
    return this.http.put(environment.appConfig.apiUrl + 'staff/' + status + '/' + type, this.getHeader());
  }
}
