import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }
  getHeader(){
    return {
        headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + (localStorage.getItem("currentUser")?JSON.parse(localStorage.getItem("currentUser"))['token']:'')
      })
    }
  }

  // Show User data
  getUserData(skip,limit,search,gender,login_type) {
    const params = {};
    params["skip"] = skip;
    params["limit"] = limit;
    if(search){
      params["search"] = search;
    }
    if(login_type){
      params["login_type"] = login_type;
    }
    if(gender){
      params["gender"] = gender;
    }
    var queryString = "?"+Object.keys(params).map(key => key + '=' + params[key]).join('&');
    return this.http.get(environment.appConfig.apiUrl + 'user/' + queryString,this.getHeader());
  }

  // Get User data Id
  getUserDataId(user_id) {
    return this.http.get(environment.appConfig.apiUrl + 'user/' + user_id, this.getHeader());
  }

  // Deleting the records
  deleteUser(user_id) {
    return this.http.delete(environment.appConfig.apiUrl + 'user/' + user_id,this.getHeader());
  }
  // status change the records
  statusChange(user_id,status) {
    return this.http.put(environment.appConfig.apiUrl + 'user/status/' + user_id,{status:status.toString()},this.getHeader());
  }
  // update User User
  updateUser(userData:any,id:any) {
    return this.http.put(environment.appConfig.apiUrl + 'user/update/' + id, userData, this.getHeader());
  }
  // update User User
  addUser(userData:any) {
    return this.http.post(environment.appConfig.apiUrl + 'user/add', userData, this.getHeader());
  }

}
