import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class BackendApiService {
  getHeader(){
    return { headers: new HttpHeaders({
      'Authorization': 'Bearer '+ JSON.parse(localStorage.getItem('currentUser')).token
    })
  }
  };
  getHeaderImage(){
    return {
        headers: new HttpHeaders({
        'Authorization': 'Bearer ' + (localStorage.getItem("currentUser")?JSON.parse(localStorage.getItem("currentUser"))['token']:'')
      })
    }
  }
  constructor(
    public http: HttpClient,
    private router: Router,
    public cs : CommonService
  ) { }

  listattendence() {
    let body = this.cs.serialization({stpp_id:0});
    return this.http.get(environment.appConfig.apiUrl + 'search?'+ body, this.getHeader() );
  }

  listdevice(){
    return this.http.get(environment.appConfig.apiUrl + 'device/list/search', this.getHeader() );
  }

  liststation(){
    return this.http.get(environment.appConfig.apiUrl + 'station-list', this.getHeader() );
  }

  listlocation(id){
    return this.http.get(environment.appConfig.apiUrl + 'station-list?parent_id=' + id, this.getHeader() );
  }

  add(newdevice):Observable<any> {
    return this.http.post(environment.appConfig.apiUrl + 'device-add', newdevice, this.getHeader());
  }


  variableadd(newdevice):Observable<any> {
    return this.http.post(environment.appConfig.apiUrl + 'variable/add', newdevice, this.getHeader());
  }

  variablelist(){
    return this.http.get(environment.appConfig.apiUrl + 'variable/list/search', this.getHeader() );
  }

  paneladd(newdevice):Observable<any> {
    return this.http.post(environment.appConfig.apiUrl + 'panel/add', newdevice, this.getHeader());
  }

  panellist(){
    return this.http.get(environment.appConfig.apiUrl + 'panel/list/search', this.getHeader() );
  }

}
